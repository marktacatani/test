<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Fibo_series Class
 *
 * @package     ExpressionEngine
 * @category    Plugin
 * @author      Mark Tacatani
 * @copyright   Copyright (c) 2015, Mark Tacatani
 * @link        http://mark.dev/
 */

$plugin_info = array(
    'pi_name'         => 'Fibonacci Series',
    'pi_version'      => '1.0',
    'pi_author'       => 'Mark Tacatani',
    'pi_author_url'   => 'http://mark.dev/',
    'pi_description'  => 'Returns a list of Fibonacci series',
    'pi_usage'        => Fibo_series::usage()
);

class Fibo_series
{

    public $return_data = "";

    // --------------------------------------------------------------------

    /**
     * Fibo_series
     *
     * This function returns a list of fibo series
     *
     * @access  public
     * @return  string
     */
    public function __construct()
    {
       $param = ee()->TMPL->fetch_param('number');

       if ( is_numeric($param) ) {
         $this->return_data = $this->fibo_calc($param);
       } else {
         $this->return_data = "Not a number";
       }

       return $this->return_data;
    }

    public function fibo_calc($param) {
      $n = 0; $first = 0; $second = 1;

      $series = "Fibonacci series of ".$param.":<br/>";
      //$series .= "0 <br/>";

      for ($i=0; $i <= $param; $i++) {
        if ($i>1) {
          $n = $first + $second;
          $first = $second;
          $second = $n;
          $series .= $n."<br/>";
        } else {
          $series .= $i."<br/>";
        }
      }
      return $series;
    }

    public function output(){
      echo $this->return_data;
    }

    // --------------------------------------------------------------------

    /**
     * Usage
     *
     * This function describes how the plugin is used.
     *
     * @access  public
     * @return  string
     */
    public static function usage()
    {
        ob_start();  ?>

The Fibo_series Plugin simply outputs a
list of fibonacci series based on the given parameter.

    {exp:memberlist}

This is an incredibly simple Plugin.


    <?php
        $buffer = ob_get_contents();
        ob_end_clean();

        return $buffer;
    }
    // END
}
/* End of file pi.memberlist.php */
/* Location: ./system/expressionengine/third_party/memberlist/pi.memberlist.php */
